﻿using UnityEngine;
using System.Collections;
using System;



// Bitmaskdatentyp für die Ausgänge eines einzelen Felds
/// <summary>
/// 
/// Ausgänge folgendermaßen aufgebaut
/// 
/// x0x
/// 3x1
/// x2x
/// 
/// 
/// 
/// 
/// </summary>
[Flags]
public enum Exits
{
    Top = (1 << 0),
    Right = (1 << 1),
    Bottom = (1 << 2),
    Left = (1 << 3)
}



public enum CubeDirections
{
    Above,
    Right,
    Below,
    Left
}



//Daten die ein einzelner Punkt haben kann (später von Rubiks-Cube Flächen übergeben)
[Serializable]
public struct WorldPointData
{
    public bool walkable;
    [EnumFlagsAttribute]
    public Exits Exit;
    [EnumFlagsAttribute]
    public Exits Entrance;
    public bool hasRotationSwitch;
    public CubeDirections SwitchSide;
    public bool isGoal;
}




/// <summary>
/// Klasse für die Verwaltung eines einzelnen Punktes des 3x3 Gitternetzes
/// </summary>
public class WorldPoint : MonoBehaviour {
    public WorldPointData Data;
    public int ID;
    public Vector2 Position;



    /// <summary>
    /// Initialisierung
    /// </summary>
    void Awake()
    {
        Data = new WorldPointData();
        Position = new Vector2(this.transform.localPosition.x, this.transform.localPosition.y);

    }


    /// <summary>
    /// Mehr Initialisierung
    /// </summary>
    void Start()
    {
        closeInvalidExits();

    }


    void closeInvalidExits()
    {
        // Checken, ob unter dem aktuellen Punkt eine Punkt ist, wenn ja hat er unten einen Ausgang
        if (!checkForAdjacentCube(CubeDirections.Below))
        {
            Data.Exit &= ~Exits.Bottom;
        }
        // Checken, ob über dem aktuellen Punkt ein Punkt ist, wenn ja hat er oben einen Ausgang
        if (!checkForAdjacentCube(CubeDirections.Above))
        {
            Data.Exit &= ~Exits.Top;
        }
        // Checken, ob der aktuelle Punkt nicht in der linken Reihe ist, wenn ja hat er links einen Ausgang
        if (!checkForAdjacentCube(CubeDirections.Left)) 
        {
            Data.Exit &= ~Exits.Left;
        }
        // Checken, ob der aktuelle Punkt nicht in der rechten Reihe ist, wenn ja hat er rechts einen Ausgang
        if (!checkForAdjacentCube(CubeDirections.Right)) 
        {
            Data.Exit &= ~Exits.Right;
        }
        Data.Entrance |= Data.Exit;
    }


    public void Init(WorldPointData data)
    {
        this.Data = data;
        closeInvalidExits();
        Debug.Log(this.gameObject.name);
        
    }




    /// <summary>
    /// Führt alle zugewiesenen Funktionen des aktuellen Punkts aus
    /// </summary>
    public void ExecutePointFunction()
    {
        if (this.Data.hasRotationSwitch)
        {
            RotateAdjacentCubeside(Data.SwitchSide);
        }
    }


    /// <summary>
    /// Rotiert die in eine angegebene Richtung anliegende Seite des Rubiks-Cubes
    /// </summary>
    /// <param name="dir">Richtung in die nach einer anliegenden Seite gechecked werden soll.</param>
    public void RotateAdjacentCubeside(CubeDirections dir)
    {
        if (checkForAdjacentCube(dir))
        {
            RubiksCubeManager.RubiksCubeSideGroups sideToRotate;
            switch (dir)
            {
                case CubeDirections.Above:
                    sideToRotate = ID - 5 > 0 ? RubiksCubeManager.RubiksCubeSideGroups.CenterH : RubiksCubeManager.RubiksCubeSideGroups.Top;
                    break;

                case CubeDirections.Right:
                    sideToRotate = ID % 3 == 0 ? RubiksCubeManager.RubiksCubeSideGroups.CenterV : RubiksCubeManager.RubiksCubeSideGroups.Right;
                    break;

                case CubeDirections.Below:
                    sideToRotate = ID - 2 < 0 ? RubiksCubeManager.RubiksCubeSideGroups.CenterH : RubiksCubeManager.RubiksCubeSideGroups.Bottom;
                    break;

                case CubeDirections.Left:
                    sideToRotate = (ID + 1) % 3 == 0 ? RubiksCubeManager.RubiksCubeSideGroups.CenterV : RubiksCubeManager.RubiksCubeSideGroups.Left;
                    break;
				
                default:
                    throw new System.Exception("NOT A VALID CUBE ROTATION SIDE");
            }
			
            RubiksCubeManager.RotateCubeSide(sideToRotate, true);
        }
        else
        {
            Debug.LogWarning("Adjacent side not valid for rotating");
        }
    }



    bool checkForAdjacentCube(CubeDirections dir)
    {
        switch (dir)
        {
            case CubeDirections.Above:
                return ID - 3 >= 0;

            case CubeDirections.Right:
                return (ID + 1) % 3 != 0;

            case CubeDirections.Below:
                return ID + 3 <= 8;

            case CubeDirections.Left:
                return ID % 3 != 0;
			
            default:
                throw new System.Exception("NOT A VALID CUBE ROTATION SIDE");
        }
    }
}
