﻿using UnityEngine;
using System.Collections;



public delegate void GenericEventHandler();
public delegate void KeyPressHandler(KeyCode Key);
public delegate void PointDataHandler(WorldPointData Data);



public static class Global {


    public static float RoundToDecimal(float input, float dezimalStellen)
    {
        dezimalStellen = Mathf.Pow(10f, dezimalStellen);
        return Mathf.Round(input * dezimalStellen) / dezimalStellen;
    }


}
