﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{


    public static event GenericEventHandler OnCleanupLevel;



    private static GameManager _instance;

    public static GameManager instance
    {
        get
        {

            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GameManager>();
                DontDestroyOnLoad(_instance.gameObject);
            }

                return _instance;
        }
    }



    void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(this);
            _instance = this;
        }
        else
        {
            if (this != _instance)
                Destroy(this.gameObject);
        }
    }

    void Start()
    {
        initGameManager();
    }



    public static void LoadLevel(string level) 
    {
        if (OnCleanupLevel != null)
            OnCleanupLevel();

        Application.LoadLevel(level);
    }
    public static void LoadLevel(int level)
    {
        if (OnCleanupLevel != null)
            OnCleanupLevel();

        Application.LoadLevel(level);
    }


    void LevelFinish(WorldPointData Data)
    {
        if (Data.isGoal)
            LoadLevel("level2");
    }

    void cleanup()
    {
        AvatarController.OnChangeWorldPoint -= LevelFinish;
    }

    void initGameManager()
    {
        Debug.Log("init game manager");
        AvatarController.OnChangeWorldPoint += LevelFinish;
    }


}
